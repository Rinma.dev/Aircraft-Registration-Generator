const { gitDescribeSync } = require('git-describe');
const { version } = require('../package.json');

const {readFile, writeFileSync} = require("fs");
const { resolve } = require('path');

const gitInfo = gitDescribeSync({
  dirtyMark: false,
  dirtySemver: false
});

const file = resolve(__dirname, '..', 'src', 'environments', 'environment.prod.ts');

readFile(file, 'utf-8', function(err, data) {
  let result = data.replace(
    /version: .*/,
    `version: '${version}-${gitInfo.hash}',`
  );
  writeFileSync(
    file,
    result,
    { encoding: 'utf-8' },
  );
});

