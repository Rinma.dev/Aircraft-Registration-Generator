import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Country} from '../../country.model';
import {COUNTRIES} from '../../../config/countries';

@Component({
  selector: 'app-country-selector',
  templateUrl: './country-selector.component.html',
  styleUrls: ['./country-selector.component.css']
})
export class CountrySelectorComponent implements OnInit {
  @Output() countrySelectedEvent: EventEmitter<Country> = new EventEmitter();

  countries: Country[] = [];
  filteredCountries: Country[];

  ngOnInit(): void {
    for (const [key, _] of Object.entries(COUNTRIES)) {
      this.countries.push(
        new Country(key)
      );
    }
  }

  onOptionSelected(country: Country) {
    const selectedCountry = this.countries.find(c => c.name.toLowerCase() === country.name.toLowerCase());
    this.countrySelectedEvent.emit(selectedCountry);
    this.reset();
  }

  filterCountries(filterEvent): void {
    const filterValue = filterEvent.query.toLowerCase();

    if (filterValue === '') {
      this.filteredCountries = this.countries;
    } else {
      this.filteredCountries = this.countries.filter(country =>
        country.name.toLowerCase().indexOf(filterValue) === 0
        || country.code.toLowerCase().indexOf(filterValue) === 0
      );
    }

    //Special Rules
    if (filterValue === 'usa') {
      this.filteredCountries.push(this.countries.filter(c => c.code.toLowerCase() === 'us')[0]);
    }
  }

  private reset() {
    this.filteredCountries = this.countries;
  }
}
