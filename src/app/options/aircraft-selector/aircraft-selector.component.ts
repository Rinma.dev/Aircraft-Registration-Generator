import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {AIRCRAFT_TYPES} from '../../../config/aircraft-type';
import {Country} from '../../country.model';
import {RULESET} from '../../../config/rules';
import * as RandExp from 'randexp';
import {MessageService} from 'primeng/api';

@Component({
  selector: 'app-aircraft-selector',
  templateUrl: './aircraft-selector.component.html',
  styleUrls: ['./aircraft-selector.component.css']
})
export class AircraftSelectorComponent implements OnInit, OnChanges {
  @Input() country: Country = null;

  aircraftTypes = AIRCRAFT_TYPES;
  active = false;
  rules = [];
  callsign = '';
  rulesGreaterEqualOne = false;

  constructor(private messageService: MessageService) {
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.country !== undefined) {
      this.rules = [];
      const rules = RULESET[this.country.code];
      for (const id of Object.keys(rules)) {
        this.rules.push({type: AIRCRAFT_TYPES.filter(value => value.id === Number(id))[0], rule: rules[id]});
      }
    }

    if (this.rules.length >= 1) {
      this.rulesGreaterEqualOne = true;
    }
  }

  generateCallsign(regex: string) {
    this.callsign = new RandExp(regex).gen().toUpperCase();

    const resultEl = document.getElementById('generation-result');
    resultEl.scrollIntoView();
  }

  copyToClipboard() {
    navigator.clipboard.writeText(this.callsign).then(() => {
      this.messageService.add({
        severity: 'info',
        summary: 'Copied to clipboard',
        detail: 'The generated callsign was copied to your clipboard'
      });
    }).catch(() => {
      this.messageService.add({
        severity: 'error',
        summary: 'Copy failed',
        detail: 'Your browser blocked the usage of your clipboard'
      });
    });
  }
}
