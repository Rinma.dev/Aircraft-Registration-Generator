import {Component, OnInit} from '@angular/core';
import {Country} from '../country.model';

@Component({
  selector: 'app-options',
  templateUrl: './options.component.html',
  styleUrls: ['options.component.css']
})
export class OptionsComponent implements OnInit {
  country: Country;

  constructor() {
  }

  ngOnInit(): void {
  }

  receiveCountry(country: Country) {
    this.country = country;
  }
}
