import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HeaderComponent} from './header/header.component';
import {CountrySelectorComponent} from './options/country-selector/country-selector.component';
import {AircraftSelectorComponent} from './options/aircraft-selector/aircraft-selector.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {OptionsComponent} from './options/options.component';
import {FooterComponent} from './footer/footer.component';
import {ImprintComponent} from './imprint/imprint.component';
import {ClipboardModule} from '@angular/cdk/clipboard';
import {ToolbarModule} from 'primeng/toolbar';
import {ImageModule} from 'primeng/image';
import {AutoCompleteModule} from 'primeng/autocomplete';
import {CardModule} from 'primeng/card';
import {ButtonModule} from 'primeng/button';
import {DividerModule} from 'primeng/divider';
import {ToastModule} from 'primeng/toast';
import {MessageService} from 'primeng/api';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    OptionsComponent,
    CountrySelectorComponent,
    AircraftSelectorComponent,
    FooterComponent,
    ImprintComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    ClipboardModule,
    ToolbarModule,
    ImageModule,
    AutoCompleteModule,
    FormsModule,
    CardModule,
    ButtonModule,
    DividerModule,
    ToastModule,
  ],
  providers: [
    MessageService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule {
}
