import {COUNTRIES} from '../config/countries';

export class Country {
  name: string;
  code: string;
  flagUrl: string;

  constructor(code: string) {
    this.code = code;
    this.name = COUNTRIES[code];
    this.flagUrl = `/assets/flags/${this.code.toLowerCase()}.svg`;
  }
}
